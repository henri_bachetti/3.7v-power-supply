# 3.7V POWER-SUPPLY

The purpose of this page is to explain step by step the realization of a digital adjustable 3.7 power-supply.

The power supply uses the following components :

 * an ARDUINO NANO
 * an MCP4725 module
 * a TL431 voltage reference
 * an OLED I2C display SSD1306 128x32
 * a rotary encoder ALPS EC11E
 * an LM78L05
 * an LM317
 * some passive components

### ELECTRONICS

The schema is made using KICAD.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2019/12/alimentation-37v-numerique.html

