
#include <Wire.h>
#include <Adafruit_MCP4725.h>
#include <SSD1306Ascii.h>
#include <SSD1306AsciiAvrI2c.h>
#include "encoder.h"

Adafruit_MCP4725 dac;
SSD1306AsciiAvrI2c display;
Encoder knob(2, 3, 4);

#define VOLTAGE_MIN             3.0
#define VOLTAGE_NOMINAL         3.7
#define VOLTAGE_MAX             4.2
#define OUTPUT_MIN              250   // adjust to obtain 3.0V
#define OUTPUT_MAX              1225  // adjust to obtain 4.2V
#define RW_DIGITAL1             5
#define RW_DIGITAL2             6
#define RW_DIGITAL3             7
#define RW_DIGITAL4             8
#define RW_DIGITAL5             9
#define RW_DIGITAL6             10
#define RW_DIGITAL7             11
#define RW_DIGITAL8             12
#define RW_DIGITAL9             13
#define READ_VOUT               0
#define READ_ANALOG1            1
#define READ_ANALOG2            2
#define READ_ANALOG3            3
#define READ_ANALOG4            6
#define READ_ANALOG5            7
#define VREF                    2.456 // the reference voltage of TL431
#define POWER_DIVIDER           0.5
#define ANALOG_DIVIDER          0.165
#define DEFAULT_ENCODER_VALUE   370
#define MIN_ENCODER_VALUE       300
#define MAX_ENCODER_VALUE       420

void setup()
{
  Serial.begin(115200);
  analogReference(EXTERNAL);
  Serial.println(F("3.7V Power Supply"));
  dac.begin(0x62);
  setVoltage(VOLTAGE_NOMINAL);
  display.begin(&Adafruit128x32, 0x3C);
  display.setFont(System5x7);
  printCentered("3.7V Power Supply", 0, 1);
  printLeft("OUTPUT: ", 2, 1);
  knob.begin();
  knob.write(DEFAULT_ENCODER_VALUE);
  knob.setStep(5);
  getVoltage(1, false);
}

static long actualEncoder = DEFAULT_ENCODER_VALUE;

void loop()
{
  long encoderValue;
  float voltage;

  if (!commandShell()) {
    encoderValue = knob.read();
    int button = knob.readButton();
    if (encoderValue != actualEncoder) {
      voltage = (float)encoderValue / 100;
      if (encoderValue < MIN_ENCODER_VALUE) {
        knob.write(MIN_ENCODER_VALUE);
        encoderValue = MIN_ENCODER_VALUE;
      }
      else if (encoderValue > MAX_ENCODER_VALUE) {
        knob.write(MAX_ENCODER_VALUE);
        encoderValue = MAX_ENCODER_VALUE;
      }
      actualEncoder = encoderValue;
      printVoltage((float)encoderValue / 100);
    }
    if (button == 0) {
      voltage = (float)actualEncoder / 100;
      setVoltage(voltage);
      delay(500);
      getVoltage(1, false);
    }
  }
}

#define CMD_MAX               100

bool commandShell(void)
{
  static char buf[CMD_MAX];
  static int i;

  if (!Serial.available()) {
    return false;
  }
  int c = Serial.read();
  if (c != -1) {
    if (c == '\n') {
      if (strlen(buf) == 0) {
        return;
      }
      if (!strncmp(buf, "VOUT=", 5)) {
        float voltage = atof(buf + 5);
        setVoltage(voltage);
        knob.write((int)(voltage * 100));
        actualEncoder = voltage * 100;
        delay(500);
        getVoltage(1, true);
      }
      else if (!strncmp(buf, "READ", 4)) {
        char s[CMD_MAX];
        int in;
        if (sscanf(buf, "%s %d", s, &in) != 2) {
          // invalid arguments
          Serial.println("EINVAL");
          return true;
        }
        getVoltage(in, true);
      }
      else if (!strncmp(buf, "IN", 2)) {
        int io = getGPIO(buf);
        if (io != 0) {
          pinMode(io, INPUT);
          Serial.println("OK");
        }
      }
      else if (!strncmp(buf, "OUT", 3)) {
        int io = getGPIO(buf);
        if (io != 0) {
          pinMode(io, OUTPUT);
          Serial.println("OK");
        }
      }
      else if (!strncmp(buf, "GET", 3)) {
        int io = getGPIO(buf);
        if (io != 0) {
          Serial.println(digitalRead(io));
        }
      }
      else if (!strncmp(buf, "SET", 3)) {
        int io = getGPIO(buf);
        if (io != 0) {
          digitalWrite(io, HIGH);
        }
        Serial.println("OK");
      }
      else if (!strncmp(buf, "RESET", 5)) {
        int io = getGPIO(buf);
        if (io != 0) {
          digitalWrite(io, LOW);
        }
        Serial.println("OK");
      }
      else {
        // command not supported
        Serial.println("ENOTSUP");
      }
      buf[0] = 0;
      i = 0;
      return true;
    }
    if (c != '\r') {
      if (i < CMD_MAX - 1) {
        buf[i++] = c;
        buf[i] = '\0';
      }
      else {
      // command too long
      Serial.println("ETOOBIG");
      }
    }
  }
  return true;
}

int gpioList[] = {RW_DIGITAL1, RW_DIGITAL2, RW_DIGITAL3, RW_DIGITAL4, RW_DIGITAL5, RW_DIGITAL6,
                  RW_DIGITAL7, RW_DIGITAL8, RW_DIGITAL9};

int getGPIO(char *buf)
{
  char s[10];
  int n;
  if (sscanf(buf, "%s %d", s, &n) != 2) {
    Serial.println("EINVAL");
    return true;
  }
  if (n < 1 || n > 9) {
    Serial.print("EINVAL: "); Serial.println(n);
    return 0;
  }
  return gpioList[n-1];
}

void setVoltage(float voltage)
{
  uint16_t value = (voltage - VOLTAGE_MIN) * (OUTPUT_MAX - OUTPUT_MIN) / (VOLTAGE_MAX - VOLTAGE_MIN) + OUTPUT_MIN;
//  Serial.print("voltage: "); Serial.println(value);
  dac.setVoltage(value, false);
}

#define NSAMPLES    20

int analogList[] = {READ_VOUT, READ_ANALOG1, READ_ANALOG2, READ_ANALOG3, READ_ANALOG4, READ_ANALOG5};

float getVoltage(int input, int shell)
{
  unsigned long adc = 0;

  if (input < 1 || input > 6) {
    Serial.print("EINVAL: "); Serial.println(input);
    return 0;
  }
  for (int sample = 0 ; sample < NSAMPLES ; sample++) {
    adc += analogRead(analogList[input-1]);
  }
  adc /= NSAMPLES;
  float divider = input == 1 ? POWER_DIVIDER : ANALOG_DIVIDER;
  float voltage = adc * VREF / 1023 / divider;
  if (input == 1) {
    printVoltage(voltage);
  }
  if (shell) {
    Serial.println(voltage);
  }
  return voltage;
}

void printLeft(const char *s, int y, int size)
{
  if (size == 1) {
    display.set1X();
  }
  else {
    display.set2X();
  }
  display.setCursor(0, y);
  display.print(s);
}

void printCentered(const char *s, int y, int size)
{
  size_t w;

  if (size == 1) {
    display.set1X();
  }
  else {
    display.set2X();
  }
  w = display.strWidth(s);
  display.setCursor((display.displayWidth() - w) / 2, y);
  display.print(s);
}

void printVoltage(float value)
{
  char s[20];
  size_t w;

  dtostrf(value, 2, 2, s);
  display.set1X();
  display.setCursor(80, 2);
  display.clearToEOL();
  display.print(s);
  display.print("V");
}

