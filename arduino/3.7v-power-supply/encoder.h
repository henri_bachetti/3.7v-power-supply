
#ifndef _ENCODER_H_
#define _ENCODER_H_

class Encoder
{
  public:
    uint8_t m_button;
    Encoder(uint8_t pinA, uint8_t pinB, uint8_t button); // must be interrupt pin
    void begin(void);
    void setStep(int s);
    long read(void);
    uint8_t readButton(void);
    void write(int32_t p);
};

#endif

