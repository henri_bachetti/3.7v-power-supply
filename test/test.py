#!/usr/bin/env python

# tu run this test :
# connect the ARDUINO to the PC using USB
# connect the analog inputs 2 3 4 5 6 to the output voltage
# replace the USB device '/dev/ttyUSB1' by the good one or 'COM1' or 'COMX' on a windows machine
# run :
#     to run all tests :
#     python test.py
#     to run one test :
#     run python test.py Test.test_power

import unittest
import time
import serial

class Test(unittest.TestCase):

	def sendReply(self, request):
		global ser
		ser.write(request+'\r\n')
		answer = ser.read_until('\n')
		print answer,
		return answer.strip('\r\n')

	def setUp(self):
		pass

	def tearDown(self):
		pass

	def test_power(self):
		print 'setting voltage to 3V'
		answer = self.sendReply('VOUT=3')
		voltage = float(answer)
		self.assertTrue(voltage > 2.97 and voltage < 3.03)
		print 'setting voltage to 3.7V'
		answer = self.sendReply('VOUT=3.7')
		voltage = float(answer)
		self.assertTrue(voltage > 3.67 and voltage < 3.73)
		print 'setting voltage to 4.2V'
		answer = self.sendReply('VOUT=4.2')
		voltage = float(answer)
		self.assertTrue(voltage > 4.17 and voltage < 4.23)

	def test_GPIO(self):
		print 'setting GPIO9 to OUTPUT'
		answer = self.sendReply('OUT 9')
		self.assertEqual(answer, 'OK')
		print 'setting GPIO9 to 1'
		answer = self.sendReply('SET 9')
		self.assertEqual(answer, 'OK')
		print 'getting GPIO9'
		answer = self.sendReply('GET 9')
		self.assertEqual(answer, '1')
		print 'setting GPIO9 to 0'
		answer = self.sendReply('RESET 9')
		self.assertEqual(answer, 'OK')
		print 'getting GPIO9'
		answer = self.sendReply('GET 9')
		self.assertEqual(answer, '0')

	def test_ANALOG(self):
		print 'setting voltage to 3.7V'
		answer = self.sendReply('VOUT=3.7')
		voltage = float(answer)
		self.assertTrue(voltage > 3.67 and voltage < 3.73)
		print 'reading ANALOG1'
		answer = self.sendReply('READ 1')
		voltage = float(answer)
		self.assertTrue(voltage > 3.6 and voltage < 3.8)
		print 'reading ANALOG2'
		answer = self.sendReply('READ 2')
		voltage = float(answer)
		self.assertTrue(voltage > 3.6 and voltage < 3.8)
		print 'reading ANALOG3'
		answer = self.sendReply('READ 4')
		voltage = float(answer)
		self.assertTrue(voltage > 3.6 and voltage < 3.8)
		print 'reading ANALOG4'
		answer = self.sendReply('READ 5')
		voltage = float(answer)
		self.assertTrue(voltage > 3.6 and voltage < 3.8)
		print 'reading ANALOG5'
		answer = self.sendReply('READ 5')
		voltage = float(answer)
		self.assertTrue(voltage > 3.6 and voltage < 3.8)
		print 'reading ANALOG6'
		answer = self.sendReply('READ 6')
		voltage = float(answer)
		self.assertTrue(voltage > 3.6 and voltage < 3.8)

if __name__ == "__main__":
	global ser
	ser = serial.Serial('/dev/ttyUSB1', baudrate=115200)
	ser.timeout = 3
	time.sleep(1)
	ser.read_until('\n') # read boot message
	unittest.main()

